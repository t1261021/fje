use crate::abs_tree::AbsTree;
use crate::display_style::DisplayStyle;
use crate::icon_family::IconFamily;

/**
 * Display the object in the form of a tree. E.g.:
 *
 * ```text
 * ├ o a: "a string"
 * ├ * b
 * │ ├ o 123
 * │ ├ *
 * │ │ └ o "qwq"
 * │ │ *
 * │ └ o false
 * └ o c: null
 * ```
 */
#[derive(Debug, Clone, Copy)]
pub struct TreeStyle;

impl DisplayStyle for TreeStyle {
    fn display(&self, icon: &IconFamily, value: &dyn AbsTree) -> Vec<String> {
        if value.is_leaf() {
            vec![value.text().unwrap_or_default()]
        } else {
            let [ref h @ .., ref e] = value.children().collect::<Vec<_>>()[..] else {
                return vec![];
            };
            let mut res = vec![];
            /// Implement the main functionality, to eliminate duplication.
            macro_rules! mkimpl {
                ($fmt:literal, $cont:literal, $k:expr, $v:expr) => {
                    let sub = self.display(icon, &*$v);
                    if $v.is_leaf() {
                        res.push(format!(
                            concat!($fmt, " {}{}{}"),
                            icon.get_icon($v.value()),
                            $k.map(|s| s.to_owned() + ": ").unwrap_or_default(),
                            sub[0]
                        ));
                    } else {
                        res.push(format!(
                            concat!($fmt, " {}{}"),
                            icon.get_icon($v.value()),
                            $k.unwrap_or_default()
                        ));
                        res.extend(sub.into_iter().map(|s| $cont.to_owned() + &s));
                    }
                };
            }
            for (k, v) in h {
                mkimpl!("├", "│ ", k, *v);
            }
            mkimpl!("└", "  ", e.0, e.1);
            res
        }
    }
}

#[cfg(test)]
mod test {
    use crate::abs_tree::JsonRoot;
    use crate::display_style::{DisplayStyle, TreeStyle};
    use crate::icon_family::icon_star_circ;

    #[test]
    fn simple_json() {
        let src = r#"{
        "a": 0,
        "b": {
            "s1": "qwq",
            "s2": [1, 2, 3, 4],
            "s3": false
        },
        "c": null,
        "d": [
            [4, 3, 2, 1],
            [],
            {},
            { "r1": 0, "r2": true }
        ]
        }"#;
        let res = TreeStyle.display(&icon_star_circ(), &JsonRoot::new(&serde_json::from_str(src).unwrap()));
        eprintln!(
            "{}",
            res.iter().intersperse(&"\n".to_string()).cloned().collect::<String>()
        );
        assert_eq!(
            res,
            vec![
                "├ o a: 0",
                "├ * b",
                "│ ├ o s1: \"qwq\"",
                "│ ├ * s2",
                "│ │ ├ o 1",
                "│ │ ├ o 2",
                "│ │ ├ o 3",
                "│ │ └ o 4",
                "│ └ o s3: false",
                "├ o c: null",
                "└ * d",
                "  ├ * ",
                "  │ ├ o 4",
                "  │ ├ o 3",
                "  │ ├ o 2",
                "  │ └ o 1",
                "  ├ * ",
                "  ├ * ",
                "  └ * ",
                "    ├ o r1: 0",
                "    └ o r2: true"
            ]
        );
    }
}
