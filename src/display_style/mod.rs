use crate::abs_tree::AbsTree;
use crate::icon_family::IconFamily;

/**
 * The concrete display function.
 */
pub trait DisplayStyle {
    /// Display the JSON value
    fn display(&self, icon: &IconFamily, tree: &dyn AbsTree) -> Vec<String>;
}

/// A hierarchied way of displaying the tree
pub trait HieredStyle {
    /// Show the root node
    fn show_root(&self, icon: &IconFamily, sub: Vec<Vec<String>>) -> Vec<String>;
    /// Show an inner node
    fn show_inner(&self, icon: &IconFamily, key: Option<&str>, sub: Vec<Vec<String>>) -> Vec<String>;
    /// Show a leaf node
    fn show_leaf(&self, icon: &IconFamily, key: Option<&str>, text: Option<String>) -> Vec<String>;
}

impl<T: HieredStyle> DisplayStyle for T {
    fn display(&self, icon: &IconFamily, tree: &dyn AbsTree) -> Vec<String> {
        fn display_sub<T: HieredStyle>(
            this: &T,
            key: Option<&str>,
            icon: &IconFamily,
            tree: &dyn AbsTree,
        ) -> Vec<String> {
            if tree.is_leaf() {
                this.show_leaf(icon, key, tree.text())
            } else {
                this.show_inner(
                    icon,
                    key,
                    tree.children().map(|(k, v)| display_sub(this, k, icon, &*v)).collect(),
                )
            }
        }
        self.show_root(
            icon,
            tree.children().map(|(k, v)| display_sub(self, k, icon, &*v)).collect(),
        )
    }
}

/**
 * The builtin rectangle style, can be replaced.
 *
 * See the document of [`RectStyle`]().
 */
mod rect_style;
/**
 * The builtin tree style, can be replaced.
 *
 * See the document of [`TreeStyle`]().
 */
mod tree_style;
pub use rect_style::RectStyle;
pub use tree_style::TreeStyle;
