use serde_json::Value;

use crate::abs_tree::AbsTree;
use crate::display_style::DisplayStyle;
use crate::icon_family::IconFamily;
use crate::is_inner_node;

/**
 * Display the object in the form of a rect. E.g.:
 *
 * ```text
 * ┌ o a: "a string" ──┐
 * ├ * b ──────────────┤
 * │ │ ├ o 123 ────────┤
 * │ │ ├ * ────────────┤
 * │ │ │ ├ o "qwq" ────┤
 * │ │ │ * ────────────┤
 * │ │ ├ o false ──────┤
 * ├ o c: null ────────┤
 * ├ * d ──────────────┤
 * └─┴ o true ─────────┘
 * ```
 */
#[derive(Debug, Clone, Copy)]
pub struct RectStyle;

impl RectStyle {
    /// Impl the functionality of [`DisplayStyle::display`](), to better recurse.
    fn display_impl(icon: &IconFamily, value: &Value) -> Vec<String> {
        match value {
            Value::Null => vec!["null".to_string()],
            Value::Bool(b) => vec![b.to_string()],
            Value::Number(n) => vec![n.to_string()],
            Value::String(s) => vec![format!("{s:?}")],
            Value::Array(arr) => {
                let mut res = vec![];
                for it in arr {
                    let sub = Self::display_impl(icon, it);
                    if is_inner_node(it) {
                        res.push(format!("├ {}", icon.get_icon(it)));
                        res.extend(sub.into_iter().map(|s| "│ ".to_owned() + &s));
                    } else {
                        res.push(format!("├ {}{}", icon.get_icon(it), sub[0]));
                    }
                }
                res
            }
            Value::Object(obj) => {
                let mut res = vec![];
                for (k, v) in obj {
                    let sub = Self::display_impl(icon, v);
                    if is_inner_node(v) {
                        res.push(format!("├ {}{}", icon.get_icon(v), k));
                        res.extend(sub.into_iter().map(|s| "│ ".to_owned() + &s));
                    } else {
                        res.push(format!("├ {}{}: {}", icon.get_icon(v), k, sub[0]));
                    }
                }
                res
            }
        }
    }
}
impl DisplayStyle for RectStyle {
    fn display(&self, icon: &IconFamily, value: &dyn AbsTree) -> Vec<String> {
        let value = value.value();
        let mut res = Self::display_impl(icon, value);
        let len = res.iter().map(|s| s.chars().count()).fold(0, usize::max) + 2;
        match &mut res[..] {
            [] => res = vec!["┌──┐".to_string(), "└──┘".to_string()],
            [h] => {
                *h = if is_inner_node(value) {
                    "┌".to_string() + &h.chars().skip(1).collect::<String>()
                } else {
                    h.to_owned()
                } + " ──┐";
                res.push(format!("└─┴{}┘", "─".repeat(len - 2)));
            }
            [h, m @ .., e] => {
                /// make the right bars
                macro_rules! mkbar {
                    ($v:expr, $l:expr) => {
                        $v.to_owned() + " " + &"─".repeat(len - $v.chars().count() - $l)
                    };
                }
                *h = h.chars().skip(1).collect::<String>();
                *h = "┌".to_string() + &mkbar!(h, 1) + "┐";
                for it in m {
                    *it = mkbar!(it, 0) + "┤";
                }
                let (eh, et) = e
                    .split_once('├')
                    .expect("This must be an inner node and thus should have the leading");
                let ec = eh
                    .chars()
                    .skip(1)
                    .map(|c| if c == '│' { '┴' } else { '─' })
                    .collect::<String>()
                    + "┴"
                    + et;
                *e = "└".to_string() + &mkbar!(ec, 1) + "┘";
            }
        }
        res
    }
}

#[cfg(test)]
mod test {
    use crate::abs_tree::JsonRoot;
    use crate::display_style::{DisplayStyle, RectStyle};
    use crate::icon_family::icon_star_circ;

    #[test]
    fn simple_json() {
        let src = r#"{
        "a": 0,
        "b": {
            "s1": "qwq a \nstring",
            "s2": [1, 2, 3, 4],
            "s3": false
        },
        "c": null,
        "d": [
            [4, 3, 2, 1],
            [],
            {},
            { "r1": 0, "r2": 0 }
        ]
        }"#;
        let res = RectStyle.display(&icon_star_circ(), &JsonRoot::new(&serde_json::from_str(src).unwrap()));
        assert_eq!(
            res,
            vec![
                "┌ o a: 0 ────────────────────┐",
                "├ * b ───────────────────────┤",
                "│ ├ o s1: \"qwq a \\nstring\" ──┤",
                "│ ├ * s2 ────────────────────┤",
                "│ │ ├ o 1 ───────────────────┤",
                "│ │ ├ o 2 ───────────────────┤",
                "│ │ ├ o 3 ───────────────────┤",
                "│ │ ├ o 4 ───────────────────┤",
                "│ ├ o s3: false ─────────────┤",
                "├ o c: null ─────────────────┤",
                "├ * d ───────────────────────┤",
                "│ ├ *  ──────────────────────┤",
                "│ │ ├ o 4 ───────────────────┤",
                "│ │ ├ o 3 ───────────────────┤",
                "│ │ ├ o 2 ───────────────────┤",
                "│ │ ├ o 1 ───────────────────┤",
                "│ ├ *  ──────────────────────┤",
                "│ ├ *  ──────────────────────┤",
                "│ ├ *  ──────────────────────┤",
                "│ │ ├ o r1: 0 ───────────────┤",
                "└─┴─┴ o r2: 0 ───────────────┘"
            ]
        );
    }

    #[test]
    fn head_longer() {
        let src = r#"{
        "a": "a long long string",
        "b": {
            "i": 0,
            "j": 1,
            "k": 2
        }
        }"#;
        let res = RectStyle.display(&icon_star_circ(), &JsonRoot::new(&serde_json::from_str(src).unwrap()));
        assert_eq!(
            res,
            vec![
                "┌ o a: \"a long long string\" ──┐",
                "├ * b ────────────────────────┤",
                "│ ├ o i: 0 ───────────────────┤",
                "│ ├ o j: 1 ───────────────────┤",
                "└─┴ o k: 2 ───────────────────┘"
            ]
        );
    }

    #[test]
    fn no_ctnt() {
        let src = r#"{}"#;
        let res = RectStyle.display(&icon_star_circ(), &JsonRoot::new(&serde_json::from_str(src).unwrap()));
        assert_eq!(res, vec!["┌──┐", "└──┘"]);
    }

    #[test]
    fn one_liner() {
        let src = r#"{"a": 0}"#;
        let res = RectStyle.display(&icon_star_circ(), &JsonRoot::new(&serde_json::from_str(src).unwrap()));
        eprintln!(
            "{}",
            res.iter().intersperse(&"\n".to_string()).cloned().collect::<String>()
        );
        assert_eq!(res, vec!["┌ o a: 0 ──┐", "└─┴────────┘"]);
    }
}
