/*!
* A library to visualize JSON files with different styles
*/
#![warn(clippy::missing_docs_in_private_items)]
#![warn(missing_docs)] // Duplicate, but this can report the missing earlier
#![warn(missing_copy_implementations)]
#![warn(missing_debug_implementations)]
#![warn(rustdoc::all)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
#![feature(const_trait_impl)]
#![feature(iter_intersperse)]
#![feature(generic_arg_infer)]
#![feature(impl_trait_in_fn_trait_return)]

use serde_json::Value;

/// Check if the node is inner node, i.e. nonleaf nodes, like arrays and objects.
pub(crate) const fn is_inner_node(value: &Value) -> bool {
    match value {
        Value::Null | Value::Bool(_) | Value::Number(_) | Value::String(_) => false,
        Value::Array(_) | Value::Object(_) => true,
    }
}

pub mod abs_tree;

/**
 * The display style interface definition and several default style implementation.
 */
pub mod display_style;

/**
 * The interface providing icons for the display, and some default icons.
 */
pub mod icon_family;

/**
 * The CLI implementation.
 *
 * See the document of [`Cli`]().
 */
#[cfg(feature = "cli")]
pub mod cli;
