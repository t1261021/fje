use std::collections::HashMap;
use std::fmt::Debug;
use std::io::Read;
use std::{fs, io};

use anyhow::{format_err, Result};
use clap::Parser;
use serde_json::Value;

use crate::abs_tree::JsonRoot;
use crate::display_style::{DisplayStyle, RectStyle, TreeStyle};
use crate::icon_family::{icon_circ, icon_none, icon_star_circ, IconFamily};

/**
 * Implement CLI functionality
 */
#[must_use]
pub struct Cli {
    /// Available styles
    pub styles: HashMap<String, Box<dyn DisplayStyle>>,
    /// Available icon families
    pub icons: HashMap<String, IconFamily>,
    /// The default style if user doesn't provide
    pub default_style: String,
    /// The default icon family if user doesn't provide
    pub default_icon: String,
}

impl Debug for Cli {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Cli")
            .field("styles", &self.styles.iter().map(|i| i.0).collect::<Vec<_>>())
            .field("icons", &self.icons)
            .field("default_style", &self.default_style)
            .field("default_icon", &self.default_icon)
            .finish()
    }
}

/**
 * Build the CLI object.
 */
#[must_use]
pub struct Builder {
    /// Available styles
    styles: HashMap<String, Box<dyn DisplayStyle>>,
    /// Available icon families
    icons: HashMap<String, IconFamily>,
    /// The default style if user doesn't provide
    default_style: Option<String>,
    /// The default icon family if user doesn't provide
    default_icon: Option<String>,
}

impl Debug for Builder {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Cli")
            .field("styles", &self.styles.iter().map(|i| i.0).collect::<Vec<_>>())
            .field("icons", &self.icons)
            .field("default_style", &self.default_style)
            .field("default_icon", &self.default_icon)
            .finish()
    }
}

impl Default for Builder {
    fn default() -> Self {
        Self::new()
    }
}

impl Builder {
    /// Create an empty CLI builder.
    pub fn new() -> Self {
        Self {
            styles: HashMap::new(),
            icons: HashMap::new(),
            default_style: None,
            default_icon: None,
        }
    }

    /// Create a builder with the preset we used in our CLI program.
    pub fn preset() -> Self {
        Self::new()
            .extend_style([
                ("tree".to_string(), Box::new(TreeStyle)),
                ("rect".to_string(), Box::new(RectStyle)),
            ] as [(String, Box<dyn DisplayStyle>); _])
            .extend_icon([
                ("none".to_string(), icon_none()),
                ("circ".to_string(), icon_circ()),
                ("star-circ".to_string(), icon_star_circ()),
            ])
            .default_style("tree".to_string())
            .default_icon("star-circ".to_string())
    }

    /// Add a style
    pub fn style(mut self, name: String, value: impl DisplayStyle + 'static) -> Self {
        self.styles.insert(name, Box::new(value));
        self
    }

    /// Add an icon
    pub fn icon(mut self, name: String, value: IconFamily) -> Self {
        self.icons.insert(name, value);
        self
    }

    /// Add styles from an iterator
    pub fn extend_style(mut self, styles: impl IntoIterator<Item = (String, Box<dyn DisplayStyle>)>) -> Self {
        self.styles.extend(styles);
        self
    }

    /// Add icons from an iterator
    pub fn extend_icon(mut self, icons: impl IntoIterator<Item = (String, IconFamily)>) -> Self {
        self.icons.extend(icons);
        self
    }

    /// Set default style
    pub fn default_style(mut self, style: String) -> Self {
        self.default_style.replace(style);
        self
    }

    /// Set default icon
    pub fn default_icon(mut self, icon: String) -> Self {
        self.default_icon.replace(icon);
        self
    }

    /// Build the CLI object.
    ///
    /// Return `None` if it's impossible to build, e.g. no styles, or no icons are provided, or the
    /// specified style or icon not in the provided set.
    #[must_use]
    pub fn build(self) -> Option<Cli> {
        let default_style = if let Some(s) = self.default_style {
            if self.styles.contains_key(&s) {
                s
            } else {
                return None;
            }
        } else {
            self.styles.keys().next().cloned()?
        };
        let default_icon = if let Some(i) = self.default_icon {
            if self.icons.contains_key(&i) {
                i
            } else {
                return None;
            }
        } else {
            self.icons.keys().next().cloned()?
        };
        Some(Cli {
            styles: self.styles,
            icons: self.icons,
            default_style,
            default_icon,
        })
    }
}

impl Default for Cli {
    fn default() -> Self {
        Self::new()
    }
}

/// Arguments of the program
#[derive(Parser)]
struct Args {
    /// The file to read and display; when absent, read from STDIN.
    file: Option<String>,
    /// The display style to use.
    #[arg(short, long)]
    style: Option<String>,
    /// The icon family to use.
    #[arg(short, long = "icon-family")]
    icon_family: Option<String>,
}

impl Cli {
    /// Create a new default CLI object
    #[allow(clippy::missing_panics_doc)] //# The `expect` below
    pub fn new() -> Self {
        Builder::preset()
            .build()
            .expect("The preset should always be able to build")
    }

    /// Execute the CLI program.
    ///
    /// # Errors
    ///
    /// (Any possible errors?
    pub fn run(&self, args: Vec<String>) -> Result<()> {
        let Args {
            file,
            style,
            icon_family,
        } = Args::parse_from(args);
        let icon_family = icon_family.as_ref().unwrap_or(&self.default_icon);
        let icon_family = self.icons.get(icon_family).ok_or_else(|| {
            format_err!(
                "Icon family {icon_family} not provided, available icon families: {}",
                self.icons
                    .keys()
                    .map(AsRef::as_ref)
                    .intersperse(", ")
                    .collect::<String>()
            )
        })?;
        let style = style.as_ref().unwrap_or(&self.default_style);
        let style = self.styles.get(style).ok_or_else(|| {
            format_err!(
                "Display style {style} not provided, available display styles: {}",
                self.styles
                    .keys()
                    .map(AsRef::as_ref)
                    .intersperse(", ")
                    .collect::<String>()
            )
        })?;
        let file: &mut dyn Read = if let Some(file) = file {
            &mut fs::File::open(file)?
        } else {
            &mut io::stdin()
        };
        let file: Value = serde_json::from_reader(file)?;
        println!(
            "{}",
            style
                .display(icon_family, &JsonRoot::new(&file))
                .iter()
                .map(AsRef::as_ref)
                .intersperse("\n")
                .collect::<String>()
        );
        Ok(())
    }
}
