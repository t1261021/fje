# FJE

一个可视化JSON文件的小工具.

## 类图

``` plantuml
@startuml
left to right direction
skinparam linetype polyline
skinparam linetype ortho

package abs_tree {
    abstract AbsTree {
        +is_leaf() -> bool
        +text() -> Option<String>
        +children() -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree>)>>
        +value() -> &Value
    }

    class JsonRoot {
        +AbsTree::is_leaf() -> bool
        +AbsTree::text() -> Option<String>
        +AbsTree::children() -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree>)>>
        +AbsTree::value() -> &Value
        ---
        +{static}new(val: &Value) -> Self
    }
    class JsonInner {
        +AbsTree::is_leaf() -> bool
        +AbsTree::text() -> Option<String>
        +AbsTree::children() -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree>)>>
        +AbsTree::value() -> &Value
    }
    class JsonLeaf {
        +AbsTree::is_leaf() -> bool
        +AbsTree::text() -> Option<String>
        +AbsTree::children() -> Box<dyn Iterator<Item = (Option<&str>, Box<dyn AbsTree>)>>
        +AbsTree::value() -> &Value
    }
    JsonRoot --|> AbsTree
    JsonInner --|> AbsTree
    JsonLeaf --|> AbsTree
    JsonRoot o--> AbsTree
    JsonInner o--> AbsTree
}

package display_style {
    abstract DisplayStyle {
        +display(icon: &IconFamily, value: &dyn AbsTree) -> Vec<String>
    }
    note bottom of DisplayStyle
    用于显示JSON文件的接口, display函数应当返回一个数组, 其中包含输出的所有行.
    end note

    abstract HieredStyle {
        +show_leaf(icon: &IconFamily, key: &str, value: &dyn AbsTree) -> Vec<String>
        +show_inner(icon: &IconFamily, key: &str, sub: Vec<Vec<String>>) -> Vec<String>
        +show_root(icon: &IconFamily, sub: Vec<Vec<String>>) -> Vec<String>
    }
    HieredStyle --|> DisplayStyle: 实现
    HieredStyle --> AbsTree
    DisplayStyle --> AbsTree

    class RectStyle {
        +DisplayStyle::display(icon: &IconFamily, value: &dyn AbsTree) -> Vec<String>
    }
    class TreeStyle {
        +DisplayStyle::display(icon: &IconFamily, value: &dyn AbsTree) -> Vec<String>
    }
    RectStyle --|> DisplayStyle
    TreeStyle --|> DisplayStyle
}

package icon_family {
    struct IconFamily {
        +null: String
        +bool: String
        +number: String
        +string: String
        +array: String
        +object: String
        ---
        +{static}from_cats(inner: &str, leaf: &str) -> Self
        +get_icon(node: &Value) -> &str
    }
    note right of IconFamily
    我们简单地用一组数据实现图标, 因为图标基本只和数据类型有关.
    事实上, 大多数时候, 图标甚至只和节点是否有子节点有关.
    end note

    entity new_factory_from_config as "new_factory_from_config(conf: impl Read) -> serde_json::Result<impl Fn(&str) -> impl IconFamilyFactory>"

    entity icon_none as "icon_none() -> IconFamily"
    entity icon_circ as "icon_circ() -> IconFamily"
    entity icon_star_circ as "icon_star_circ() -> IconFamily"
    note "几个返回图标的函数, 由于Rust语言的限制, 它们不能直接定义为单例" as icon_fn
    icon_fn .. icon_none
    icon_fn .. icon_circ
    icon_fn .. icon_star_circ

    abstract icon_family.IconFamilyFactory {
        Fn::call() -> IconFamily
    }
    IconFamilyFactory --> IconFamily: 创建
    icon_none --|> IconFamilyFactory: 实现
    icon_circ --|> IconFamilyFactory: 实现
    icon_star_circ --|> IconFamilyFactory: 实现
    new_factory_from_config ..> IconFamilyFactory: 创建
}

package cli {
    class Cli {
        -styles: HashMap<String, Box<dyn DisplayStyle>>
        -icons: HashMap<String, IconFamily>
        -default_style: String
        -default_icon: String
        ---
        +Debug::fmt(&mut Formatter) -> fmt::Result
        ---
        +{static} Default::default() -> Self
        ---
        +{static}new() -> Self
        +run(args: Vec<String>) -> Result<()>
    }
    note top of Cli
    主要的CLI接口, 用于方便用户拓展CLI.

    我们的CLI程序也只是对这一接口的简单封装.
    end note

    class Builder {
        +Debug::fmt(&mut Formatter) -> fmt::Result
        ---
        +{static} Default::default() -> Self
        ---
        +{static} new() -> Self
        +{static} preset() -> Self
        ---
        +style(name: String, value: impl DisplayStyle + 'static) -> Self
        +icon(name: String, value: IconFamily) -> Self
        +extend_style(styles: impl IntoIterator<Item = (String, Box<dyn DisplayStyle>)>) -> Self
        +extend_icon(icons: impl IntoIterator<Item = (String, IconFamily)>) -> Self
        +default_style(style: String) -> Self
        +default_icon(icon: String) -> Self
        ---
        +build() -> Option<Cli>
    }

    display_style.DisplayStyle ..> IconFamily: 使用
    Cli ..> display_style.DisplayStyle: 使用
    Cli ..> IconFamily: 使用
    Builder --> Cli: 创建
}

@enduml
```

## 设计说明

我们使用的serde_json库提供的json Value使用了组合模式, 它的Component, Leaf和Composite都是`Value`类型本身.

我们在JsonTree中使用了组合模式, 其中, `JsonRoot` 和 `JsonInner` 是Composite, `JsonLeaf` 是Leaf, `JsonTree` 是Component.

`icon_none`, `icon_circ`和`icon_star_circ`是 IconFamily 的工厂.
`new_factory_from_config` 提供了一个从配置文件创建 IconFamilyFactory 这一工厂的工厂的方式.

`JsonRoot::new()` 是 `JsonRoot` 的静态工厂.

`cli::Builder` 是 `Cli` 的建造者.

`IconFamilyFactory` 是 `IconFamily` 的抽象工厂, 由于只是一个函数接口, 三个函数工厂自然实现了它.
